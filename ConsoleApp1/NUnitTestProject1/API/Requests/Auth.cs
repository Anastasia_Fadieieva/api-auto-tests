﻿using System.Collections.Generic;
using Newtonsoft.Json;
using NUnitTestProject1.API.Models;
using NUnitTestProject1.Support;
using RestSharp;

namespace NUnitTestProject1.API.Requests
{
    internal static class Auth
    {
        internal static AuthModel AuthPost(string email, string password)
        {
            ApiRequest.ConfigureApiRequest("https://api.newbookmodels.com/api/v1/auth/signin/");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"email", email},
                {"password", password}
            };
            ApiRequest.SetData(parameters);

            var response = ApiRequest.SendRequest();
            return JsonConvert.DeserializeObject<AuthModel>(response.Content);
        }
    }
}
